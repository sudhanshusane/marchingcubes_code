#include "IsosurfaceGenerator_OMP.h"
#include "ConnectedTriangles_OMP.h"
#include "TimingModule.h"
#include <vtkDataSetReader.h>
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>
#include <iostream>
#include <fstream> 
#include "RAJA/RAJA.hxx"
#include <sys/stat.h>

using namespace std;


bool fileExists(char *filename)
{       
	string str(filename);
	struct stat buf;
	if(stat(str.c_str(), &buf) != -1)
		return true;
	return false;
}



int main(int argc, char *argv[])
{
	if(argc != 7){
		cout << "Insufficient arguments.\n Usage : " << argv[0] << " <dataset_filename> <isoval> <fieldName> <result_filename> <merge-1/0> <output_filename>\n";
	}	
	TimingModule *totT = new TimingModule();

	vtkDataSetReader *rdr = vtkDataSetReader::New();
	rdr->SetFileName(argv[1]);
	rdr->SetScalarsName(argv[3]);
	rdr->Update();
		
	int dims[3];
	vtkRectilinearGrid *rgrid = (vtkRectilinearGrid *)rdr->GetOutput();
	rgrid->GetDimensions(dims);
	
	float *X = (float *) rgrid->GetXCoordinates()->GetVoidPointer(0);
	float *Y = (float *) rgrid->GetYCoordinates()->GetVoidPointer(0);
	float *Z = (float *) rgrid->GetZCoordinates()->GetVoidPointer(0);
	float *F = (float *) rgrid->GetPointData()->GetScalars()->GetVoidPointer(0);
	float isoVal = atof(argv[2]);
	int merge = atoi(argv[5]);
	bool mergepoints;	

	if(merge == 1)
		mergepoints = true;	
	else
		mergepoints = false;

	totT->StartTimer();

/*
 * Isosurface Generator generates a set of points and edges. These points and edges contain duplicates.
 */
	IsosurfaceGenerator *isoGen = new IsosurfaceGenerator();
	isoGen->GenerateIsosurface(X,Y,Z,F,dims,isoVal);
	
/*
 * Connected Triangles merges the duplicate points to generate a list of unique points with the correct set of faces. 
 */
	ConnectedTriangles *ct = new ConnectedTriangles();
	if(mergepoints == true){
		ct->GenerateUniquePoints(isoGen->GetPointList(), isoGen->GetEdgeList(), isoGen->GetNumberOfPoints(), 0, dims[0], 0, dims[1], 0, dims[2]);
	}
	
	totT->EndTimer();
	totT->AccumulateTotalTime();
	
/*
 * Write the set of vertices and faces to a file. 
 */
		
	vtkPolyData *pd = isoGen->MakePolyData(ct->GetFaceList(), ct->GetUniquePoints(), ct->GetNumberOfFaces(), ct->GetNumberOfUniquePoints());
	
	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetFileName(argv[6]);

	writer->SetInputData(pd);
	writer->SetDataModeToAscii();
	writer->Write();
/*
 * Generate timings of individual sections of the code and report them in a results file. 
 */

	double visitTime = isoGen->GetVisitTime() + ct->GetVisitTime();
	double memTime = isoGen->GetMemoryTime() + ct->GetMemoryTime();	
	double ccTime = isoGen->GetCCTime();
	double epTime = isoGen->GetVisitTime();
        double mgTime = ct->GetVisitTime();
	double sortTime = ct->GetSortTime();
	
	ofstream resFile;

	if(fileExists(argv[4])){
	resFile.open(argv[4], fstream::app|fstream::out);
	}
	else{
	resFile.open(argv[4], fstream::app|fstream::out);
	resFile << "\n"
	<< setw(15) << left << "Implementation"
	<< setw(15) << left << "IsoValue"
	<< setw(15) << left << "Time"
	<< setw(15) << left << "Visit"
	<< setw(15) << left << "Memory Time"
	<< setw(15) << left << "HtoD"
	<< setw(15) << left << "DtoH"
	<< setw(15) << left << "Cell Ctg"
	<< setw(15) << left << "EdgePoint Time"
	<< setw(15) << left << "Merging Time"
	<< setw(15) << left << "Sort Time"
	<< setw(15) << left << "CopyBack Time"
	<< setw(15) << left << "Merge"
	<< setw(40) << left << "Dataset";
	}

	resFile << "\n"
	<< setw(15) << left << "RAJA-OMP"
	<< setw(15) << left << isoVal
	<< setw(15) << left << totT->GetTotalTime()
	<< setw(15) << left << visitTime
	<< setw(15) << left << memTime
	<< setw(15) << left << "NA"
	<< setw(15) << left << "NA"
	<< setw(15) << left << ccTime
	<< setw(15) << left << epTime
	<< setw(15) << left << mgTime
	<< setw(15) << left << sortTime
	<< setw(15) << left << "NA"
	<< setw(15) << left << mergepoints
	<< setw(40) << left << argv[1];
		
	delete isoGen;
}

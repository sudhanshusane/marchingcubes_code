#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkDataArray.h>
#include "triTable.cxx"
#include "interpolateTable.cxx"
#include "numTrianglesTable.cxx"
#include "RAJA/RAJA.hxx"
#include <omp.h>
#include <iostream>
#include "TimingModule.h"
#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/system/omp/execution_policy.h>

using namespace std;

class IsosurfaceGenerator
{
	
	float* points;
	int* edges;
	int totalNumVertices;
	int maxFaces;
	int maxPoints;
	typedef RAJA::omp_parallel_for_exec execute_policy;
	TimingModule *parT, *memT, *visT, *ccT;  	
	int totalPts;

	public:
		IsosurfaceGenerator(){
			parT = new TimingModule();
			memT = new TimingModule();
			visT = new TimingModule();
			ccT = new TimingModule();
		}
		
		~IsosurfaceGenerator(){
			delete [] points;
			delete [] edges;
		}

		void GenerateIsosurface(float *X, float *Y, float *Z, float *F, int *dims, float isoVal){

		
			totalPts = dims[0]*dims[1]*dims[2];

			int numberOfCells = GetNumberOfCells(dims);
			
			memT->StartTimer();
			int *numFacesPerCell = new int[numberOfCells];
			int *validCell = new int[numberOfCells];
			int *validCellEnum = new int[numberOfCells];
			memT->EndTimer();
			memT->AccumulateTotalTime();
		

			omp_set_num_threads(16);	
			ccT->StartTimer();	
			RAJA::forall<execute_policy>(0, (dims[2]-1), [=] (int k){
			//	RAJA::forall<execute_policy>(0, (dims[1]-1), [=] (int j){
				for(int j=0; j < dims[1]-1; j++){
					for(int i=0; i < dims[0]-1; i++){
						int cellIndex = i + j*(dims[0]-1) + k*(dims[0]-1)*(dims[1]-1);
						int icase, flag = 0;
						icase = IdentifyCellCase(i,j,k,dims,isoVal,F);
						numFacesPerCell[cellIndex] = numTriangles[icase];   
							
						if(numTriangles[icase] > 0){
							flag = 1;
						}
						validCell[cellIndex] = flag;
					}
				}//);
			 });

			thrust::device_ptr<int> dev_validCell(validCell);
			thrust::device_ptr<int> dev_validCellEnum(validCellEnum);

			thrust::inclusive_scan(thrust::omp::par, dev_validCell, dev_validCell + numberOfCells, dev_validCellEnum);


			int numberOfValidCells = validCellEnum[numberOfCells-1];
			memT->StartTimer();
			int *validCellIndices = new int[numberOfValidCells];
			int *numberOfTrianglesCompacted = new int[numberOfValidCells];
			int *numberOfTrianglesEnum = new int[numberOfValidCells];
			memT->EndTimer();
			memT->AccumulateTotalTime();
		
			RAJA::forall<execute_policy>(0, numberOfCells, [=] (int i){
				if(i != 0 && i > 0){
					if(validCellEnum[i] > validCellEnum[i-1])
						validCellIndices[validCellEnum[i-1]] = i;
				}		
				else{
					validCellIndices[0] = i;
				}
			});

	
			RAJA::forall<execute_policy>(0, numberOfValidCells, [=] (int i){
				numberOfTrianglesCompacted[i] = numFacesPerCell[validCellIndices[i]];
			});
			

			thrust::device_ptr<int> dev_numberOfTrianglesCompacted(numberOfTrianglesCompacted);
			thrust::device_ptr<int> dev_numberOfTrianglesEnum(numberOfTrianglesEnum);
			
			
			thrust::exclusive_scan(thrust::omp::par, dev_numberOfTrianglesCompacted, dev_numberOfTrianglesCompacted + numberOfValidCells, dev_numberOfTrianglesEnum);	


/*			numberOfTrianglesEnum[0] = 0;
			for(int i = 1 ; i < numberOfValidCells; i++)
				numberOfTrianglesEnum[i] = numberOfTrianglesEnum[i-1] + numberOfTrianglesCompacted[i];  
*/
			

			totalNumVertices = (numberOfTrianglesEnum[numberOfValidCells - 1] +
						 numberOfTrianglesCompacted[numberOfValidCells-1])*3; 


			// Since it was an exclusive scan, to calculate the size of points array,
			//  we need to add the last count of triangles.

			ccT->EndTimer();
			ccT->AccumulateTotalTime();
		
			memT->StartTimer();
	
			points = new float[totalNumVertices*3]; // x,y,z coordinates
			edges = new int[totalNumVertices*2];

			memT->EndTimer();
			memT->AccumulateTotalTime();

			
			visT->StartTimer();
			RAJA::forall<execute_policy>(0, numberOfValidCells, [=] (int i){
				float bbox[6];
				float v[8];
				int logIdx[3];
				int edge1, edge2, edge3;
				int e = 0;
				float pt1[3], pt2[3], pt3[3];
				int icase;
			//	int ids[8];

				int cellId = validCellIndices[i];
				BoundingBoxForCell(X,Y,Z,dims, cellId, bbox);	
				GetLogicalCellIndex(logIdx, cellId, dims);
				GetPointValues(logIdx[0], logIdx[1], logIdx[2], F, v, dims);
			//	GetPointId(ids, dims, logIdx[0], logIdx[1], logIdx[2]);
				icase = IdentifyCellCase(v, isoVal);
				int edgeIndex = numberOfTrianglesEnum[i]*6;
				int startIndex = numberOfTrianglesEnum[i]*9;
				while(triCase[icase*16 + e] != -1){
					edge1 = triCase[icase * 16 + (e+0)];
					edge2 = triCase[icase * 16 + (e+1)];
					edge3 = triCase[icase * 16 + (e+2)];
					e = e + 3;
					UpdateEdges(dims, edge1, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 0);
					UpdateEdges(dims, edge2, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 2);
					UpdateEdges(dims, edge3, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 4);
					edgeIndex += 6;
					InterpolatePosition(edge1, pt1, bbox, v, isoVal);
					InterpolatePosition(edge2, pt2, bbox, v, isoVal);
					InterpolatePosition(edge3, pt3, bbox, v, isoVal);
					AddPoint(pt1, startIndex);
					startIndex += 3;    
					AddPoint(pt2, startIndex);
					startIndex += 3;
					AddPoint(pt3, startIndex);
					startIndex += 3;
				}					
				
			});
			visT->EndTimer();
			visT->AccumulateTotalTime();
				
			memT->StartTimer();
			delete [] validCell;
			delete [] validCellEnum;
			delete [] numFacesPerCell;
			delete [] numberOfTrianglesCompacted;
			delete [] validCellIndices;	
			delete [] numberOfTrianglesEnum;
			memT->EndTimer();
			memT->AccumulateTotalTime();
		}

		inline int GetNumberOfPoints(){
			return totalNumVertices;
		}
		
		inline int GetNumberOfGridPoints(){
			return totalPts;
		}
		
		inline int GetNumberOfFaces(){
			return (totalNumVertices/3);
		}
		
		inline float* GetPointList(){
			return points;
		}
		
		inline int* GetEdgeList(){
			return edges;
		}
		
		inline float GetCCTime(){
			return ccT->GetTotalTime();
		}


		inline float GetVisitTime(){
			return visT->GetTotalTime();
		}

		inline float GetMemoryTime(){
			return memT->GetTotalTime();
		}

		inline float GetParallelTime(){
			return parT->GetTotalTime();
		}

		inline vtkPolyData* MakePolyData(int *faces, float *uniquePoints, int numFaces, int numUniquePoints)
		{
			vtkPoints *vtk_pts = vtkPoints::New();
			vtk_pts->SetNumberOfPoints(numUniquePoints);
				
			vtkCellArray *triangles = vtkCellArray::New();
			triangles->EstimateSize(numFaces, 3);
			
					
			RAJA::forall<execute_policy>(0, numUniquePoints, [=] (int i){
				float pt[3];
				pt[0] = uniquePoints[i*3+0];
				pt[1] = uniquePoints[i*3+1];
				pt[2] = uniquePoints[i*3+2];
				vtk_pts->SetPoint(i, pt);
			});

	
			for(int i = 0; i < numFaces ; i++ ){
				vtkIdType ids[3] = {faces[i*3+0], faces[i*3+1], faces[i*3+2]};
				triangles->InsertNextCell(3, ids);
			}	
			
			vtkPolyData *pd = vtkPolyData::New();
			pd->SetPoints(vtk_pts);
			pd->SetPolys(triangles);
			
			triangles->Delete();
			vtk_pts->Delete();
			
			return pd;
		}
	
	private:

		inline void AddPoint(float *pt, int idx)
		{
			points[idx+0] = pt[0];
			points[idx+1] = pt[1];
			points[idx+2] = pt[2];
		
		}

		inline int IdentifyCellCase(int x, int y, int z, int *dims, float isoVal, const float *F)
		{
			float values[8];
			GetPointValues(x, y, z, F, values, dims);
			return IdentifyCellCase(values, isoVal);			
		}		


		inline int IdentifyCellCase(float *values, float isoVal)
		{
			float v[8] = {0};
			for(int i = 0; i < 8 ; i ++)
			        if(values[i] > isoVal)
			                v[i] = 1;

			return (v[7]*128 + v[6]*64 + v[5]*32 + v[4]*16 + v[3]*8 + v[2]*4 + v[1]*2 + v[0]*1);
		}		


		inline void GetPointValues(int x, int y, int z, const float *F, float *values, int *dims)
		{
			values[0] = F[z*dims[0]*dims[1] + y*dims[0] + x];
			values[1] = F[z*dims[0]*dims[1] + y*dims[0] + (x+1)];
			values[2] = F[(z)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1)];
			values[3] = F[(z)*dims[0]*dims[1] + (y+1)*dims[0] + (x)];
			values[4] = F[(z+1)*dims[0]*dims[1] + y*dims[0] + x];
			values[5] = F[(z+1)*dims[0]*dims[1] + y*dims[0] + (x+1)];
			values[6] = F[(z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1)];
			values[7] = F[(z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x];
		}

		inline void GetPointId(int *ids, int *dims, int x, int y, int z){
			ids[0] = z*dims[0]*dims[1] + y*dims[0] + x;
			ids[1] = z*dims[0]*dims[1] + y*dims[0] + (x+1);
			ids[2] = (z)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
			ids[3] = (z)*dims[0]*dims[1] + (y+1)*dims[0] + (x);
			ids[4] = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
			ids[5] = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
			ids[6] = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
			ids[7] = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
		
		}

		inline void InterpolatePosition(int edge, float *pt, float *bbox, float *v, float isoVal)
		{

		float bbox_1 = bbox[ip[edge*8+0]];
		float bbox_2 = bbox[ip[edge*8+1]];
		float bbox_3 = bbox[ip[edge*8+2]];
		float bbox_4 = bbox[ip[edge*8+3]];
		float bbox_5 = bbox[ip[edge*8+4]];
		float bbox_6 = bbox[ip[edge*8+5]];
		float v_1 = v[ip[edge*8+6]];
		float v_2 = v[ip[edge*8+7]];

		GetPositionOnEdge(bbox_1, bbox_2, bbox_3, bbox_4, bbox_5, bbox_6, v_1, v_2, pt, isoVal);

		}

		
		inline void GetPositionOnEdge(float Ax, float Bx, float Ay, float By, float Az, float Bz, float FA, float FB, float *pt, float isoVal)
		{
			float dif1 = (isoVal - FA);
			float dif2 = (FB - FA);
			float t = dif1/dif2;

			pt[0] = t*(Bx-Ax) + Ax;
			pt[1] = t*(By-Ay) + Ay;
			pt[2] = t*(Bz-Az) + Az;
		}

		
		inline void BoundingBoxForCell(const float *X, const float *Y, const float *Z, const int *dims, int cellId, float *bbox)
		{
       			if(cellId >= 0 && cellId < GetNumberOfCells(dims)){
                		int idx[3];
              			GetLogicalCellIndex(idx, cellId, dims);
              			bbox[0] = X[idx[0]];
               			bbox[1] = X[idx[0]+1];
                		bbox[2] = Y[idx[1]];
                		bbox[3] = Y[idx[1]+1];
                		bbox[4] = Z[idx[2]];
                		bbox[5] = Z[idx[2]+1];
        		}
        		else{
		                bbox[0] = -10000;
       			        bbox[1] = +10000;
	      	 	        bbox[2] = -10000;
		                bbox[3] = +10000;
                		bbox[4] = -10000;
		                bbox[5] = +10000;
       			 }
		}
		
		inline int GetNumberOfCells(const int *dims)
		{       
        		return (dims[0]-1)*(dims[1]-1)*(dims[2]-1);
		}	


		inline void GetLogicalCellIndex(int *idx, int cellId, const int *dims)
		{       
		        idx[0] = cellId%(dims[0]-1);
		        idx[1] = (cellId/(dims[0]-1))%(dims[1]-1);
		        idx[2] = cellId/((dims[0]-1)*(dims[1]-1));
		}


		inline void UpdateEdges(int *dims, int edge1, int x, int y, int z, int edgeIndex, int offset){
		int ptId1, ptId2;
		if(edge1 == 0){
			ptId1= z*dims[0]*dims[1] + y*dims[0] + x;
		       	ptId2= z*dims[0]*dims[1] + y*dims[0] + (x+1);
		}
		else if(edge1 == 1){
		        ptId1 = z*dims[0]*dims[1] + y*dims[0] + (x+1);
		        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		}
		else if(edge1 == 2){
		        ptId1 = z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + x;
		}
		else if(edge1 == 3){
		        ptId1 = z*dims[0]*dims[1] + y*dims[0] + x;
		        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + x;
		}
		else if(edge1 == 4){
		       ptId1 = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
		       ptId2 = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
		}
		else if(edge1 == 5){
			ptId1 = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
			ptId2= (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		}
		else if(edge1 == 6){
		      ptId1  =     (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		      ptId2  = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
		}
		else if(edge1 == 7){
		     ptId1   =     (z+1)*dims[0]*dims[1] + y*dims[0] + x;
		     ptId2   = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
		}
		else if(edge1 == 8){
		      ptId1  =     z*dims[0]*dims[1] + y*dims[0] + x;
		      ptId2  = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
		}
		else if(edge1 == 9){
		      ptId1  =     z*dims[0]*dims[1] + y*dims[0] + (x+1);
		      ptId2  = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
		}
		else if(edge1 == 10){
		      ptId1  =     z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		      ptId2  = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
		}
		else if(edge1 == 11){
		   ptId1     =     z*dims[0]*dims[1] + (y+1)*dims[0] + x;
		   ptId2     = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
		}

		if( ptId1 > ptId2)
		{
			int temp =ptId1;
			ptId1 = ptId2;
			ptId2 = temp;
		}
	        edges[edgeIndex+0+offset] = ptId1;
	        edges[edgeIndex+1+offset] = ptId2;
	}
		 
		
};


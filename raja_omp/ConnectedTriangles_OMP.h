#include <utility>
#include <cmath>
#include <iostream>
#include "RAJA/RAJA.hxx"
#include <omp.h>
#include <thrust/sort.h>
#include <thrust/system/cpp/execution_policy.h>
#include "TimingModule.h"
#include <thrust/device_ptr.h>

using namespace std;

class ConnectedTriangles
{

	struct VertexInfo{
		int index, v1, v2;	
	};

	
	int *faces;
	float *uniquePoints;
	VertexInfo *vertices;
	unsigned int *mc;
	int totalUniquePoints;
	int totalNumFaces;
	typedef RAJA::omp_parallel_for_exec execute_policy;
	TimingModule *parT, *memT, *sortT, *visT;

	public:
	
	ConnectedTriangles(){
		totalUniquePoints = 0;
		totalNumFaces = 0;
		sortT = new TimingModule();
		parT = new TimingModule();
		memT = new TimingModule();
		visT = new TimingModule();
	}

	~ConnectedTriangles(){
		delete [] faces;
		delete [] uniquePoints;
	}
	
	void GenerateUniquePoints(float *points, int *edges, int numPoints, float minX, float maxX, float minY, float maxY, float minZ, float maxZ){
		
		visT->StartTimer();	
	
		memT->StartTimer();
		vertices = new VertexInfo[numPoints];
		mc = new unsigned int[numPoints];	
		int *flagDuplicates = new int[numPoints];		
		int *pointIndicesEnum = new int[numPoints];
		faces = new int[numPoints];
		memT->EndTimer();
		memT->AccumulateTotalTime();

		totalNumFaces = numPoints/3;
		
		omp_set_num_threads(16);
	
		RAJA::forall<execute_policy>(0, numPoints, [=] (int i){
			mc[i] = uniquePair(edges[i*2+0], edges[i*2+1]);
			vertices[i].v1 = edges[i*2+0];
			vertices[i].v2 = edges[i*2+1];
			vertices[i].index = i;			
		});
	
		omp_set_num_threads(16);

		sortT->StartTimer();
	

		sort(mc, vertices, numPoints);
		sortT->EndTimer();
		sortT->AccumulateTotalTime();
		
		omp_set_num_threads(16);
	
		
		RAJA::forall<execute_policy>(0, (numPoints-1), [=] (int i){
			int flag = 1;
			if(mc[i] == mc[i+1])
				if(vertices[i].v1 == vertices[i+1].v1 && vertices[i].v2 == vertices[i+1].v2 ||
				   vertices[i].v1 == vertices[i+1].v2 && vertices[i].v2 == vertices[i+1].v1)
					flag = 0;
			flagDuplicates[i+1] = flag;
			if(i == 0)
				flagDuplicates[0] = 1;
		});

		
		thrust::device_ptr<int> dev_flagDuplicates(flagDuplicates);
		thrust::device_ptr<int> dev_pointIndicesEnum(pointIndicesEnum);

		thrust::exclusive_scan(thrust::cpp::par, dev_flagDuplicates, dev_flagDuplicates + numPoints, dev_pointIndicesEnum);	

		totalUniquePoints = pointIndicesEnum[numPoints - 1];

		RAJA::forall<execute_policy>(0, numPoints, [=] (int i){
			int faceNo = floor(vertices[i].index / 3);	
			int vertexNo = vertices[i].index % 3;
			faces[faceNo*3+vertexNo] = pointIndicesEnum[i];
		});

		memT->StartTimer();	
		int *uniquePointIndices = new int[totalUniquePoints];
		uniquePoints = new float[totalUniquePoints*3];
		memT->EndTimer();
		memT->AccumulateTotalTime();		

		
		int current = -1;
	
		for(int i = 0; i < numPoints; i++)
		{
			if(pointIndicesEnum[i] > current){
				current = pointIndicesEnum[i];
				uniquePointIndices[current] = i;
			}
		}

		RAJA::forall<execute_policy>(0, totalUniquePoints, [=] (int i){
			int idx1 = uniquePointIndices[i];
			int idx2 = vertices[idx1].index;
			uniquePoints[i*3 + 0] = points[idx2*3 + 0];
			uniquePoints[i*3 + 1] = points[idx2*3 + 1];
			uniquePoints[i*3 + 2] = points[idx2*3 + 2];
		});
		
		memT->StartTimer();	
		delete [] flagDuplicates;
		delete [] pointIndicesEnum;
	//	delete [] uniquePointIndices;
		delete [] vertices;
		memT->EndTimer();
		memT->AccumulateTotalTime();
		visT->EndTimer();
		visT->AccumulateTotalTime();
	}

	int* GetFaceList(){
		return faces;
	}

	float* GetUniquePoints(){
		return uniquePoints;
	}

	int GetNumberOfUniquePoints(){
		return totalUniquePoints;
	}

	int GetNumberOfFaces(){
		return totalNumFaces;
	}
	float GetVisitTime(){
		return visT->GetTotalTime();
	}

	float GetParallelTime(){
		return parT->GetTotalTime();
	}
	
	float GetSortTime(){
		return sortT->GetTotalTime();
	}
		
	float GetMemoryTime(){
		return memT->GetTotalTime();
	}
		
	private:
		
		inline void sort(unsigned int* mc, VertexInfo *vertices, int n){
			
			thrust::device_ptr<unsigned int> dev_mc(mc);
			thrust::device_ptr<VertexInfo> dev_vertices(vertices);

			thrust::sort_by_key(thrust::omp::par, dev_mc, dev_mc + n, dev_vertices);
			//thrust::sort_by_key(thrust::omp::par, mc, mc + n, vertices);
		}

			
		inline int uniquePair(int x, int y){		
			if(x > y)
				return (x*x) + x + y;
			else 
				return (y*y) + x; 
		}
	
};

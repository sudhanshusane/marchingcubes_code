#include <iostream>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/filter/MarchingCubes.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/DeviceAdapter.h>
#include <vtkm/cont/testing/Testing.h>
#include <vtkm/cont/DynamicCellSet.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderRectilinear.h>
#include <vtkm/cont/DataSetBuilderExplicit.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <sys/stat.h>
#include <iomanip>
#include "TimingModule.h"
#include <vtkDataSetReader.h>
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>

#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_TBB

using namespace std;

bool fileExists(char *filename)
{
	string str(filename);
	struct stat buf;
	if(stat(str.c_str(), &buf) != -1)
		return true;
	return false;
}


inline vtkm::cont::DataSet Make3DRectilinearDataSet(char *file_name, char *var_name){
vtkDataSetReader *rdr = vtkDataSetReader::New();
rdr->SetFileName(file_name);
rdr->SetScalarsName(var_name);
rdr->Update();

int dims[3];
vtkRectilinearGrid *rgrid = (vtkRectilinearGrid *)rdr->GetOutput();
rgrid->GetDimensions(dims);

float *X = (float *)rgrid->GetXCoordinates()->GetVoidPointer(0);
float *Y = (float *)rgrid->GetYCoordinates()->GetVoidPointer(0);
float *Z = (float *)rgrid->GetZCoordinates()->GetVoidPointer(0);
float *F = (float *)rgrid->GetPointData()->GetScalars()->GetVoidPointer(0);

vtkm::cont::DataSetBuilderRectilinear dsb;
vector<vtkm::Float32> x, y, z;

for(int i = 0 ; i < dims[0]; i++)
	x.push_back(X[i]);

for(int i = 0 ; i < dims[1]; i++)
	y.push_back(Y[i]);

for(int i = 0 ; i < dims[2]; i++)
	z.push_back(Z[i]);

//x.insert(x.begin(), &X[0], &X[dims[0]-1]);
//y.insert(y.end(), &Y[0], &Y[dims[1]-1]);
//z.insert(z.end(), &Z[0], &Z[dims[2]-1]);

vtkm::cont::DataSet dataSet = dsb.Create(x, y, z);
vtkm::cont::DataSetFieldAdd dsf;

const vtkm::Id nVerts = dims[0]*dims[1]*dims[2];
vtkm::Float32 var[nVerts];

for(int i = 0; i < nVerts; i++)
	var[i] = F[i];

dsf.AddPointField(dataSet, var_name, var, nVerts);

return dataSet;

}



int main(int argc, char *argv[])
{
cout << "VTKM DEVICE ADAPTER IS :" << VTKM_DEVICE_ADAPTER << endl;
vtkm::cont::DataSet inputData; 
vtkm::Float32 isovalue;
std::string fieldName;
TimingModule *totalTime, *isoGenTime; 
isoGenTime = new TimingModule();
totalTime = new TimingModule();
int merge;

//cout << "Using : " << argv[1] << " as MarchingCubes input file. " << endl;
totalTime->StartTimer();
inputData = Make3DRectilinearDataSet(argv[1], argv[3]);
//inputData.PrintSummary(cout);
isovalue = atof(argv[2]);
fieldName = argv[3];
merge = atoi(argv[5]);
bool mergepoints;
if(merge == 1)
mergepoints = true;
else
mergepoints = false;

isoGenTime->StartTimer();
vtkm::filter::MarchingCubes filter;
filter.SetGenerateNormals(false);
filter.SetMergeDuplicatePoints(mergepoints); // This can be set to true and then the difference in the timing is the time taken to merge the points approximately. 
filter.SetIsoValue(isovalue);
vtkm::filter::ResultDataSet result = filter.Execute(inputData, inputData.GetField(fieldName) );
filter.MapFieldOntoOutput(result, inputData.GetField(fieldName) );

double visitTime = filter.GetVisitTime();
double ccTime = filter.GetCCTime();
double epTime = filter.GetEPTime();
double mgTime = filter.GetMgTime();
double sortTime = filter.GetSortTime();

vtkm::cont::DataSet& outputData = result.GetDataSet();
//outputData.PrintSummary(cout);
//vtkm::cont::DynamicCellSet cellSetData = outputData.GetCellSet();

//cout << "Number of Cells : " << cellSetData.GetNumberOfCells();
isoGenTime->EndTimer();
isoGenTime->AccumulateTotalTime();

totalTime->EndTimer();
totalTime->AccumulateTotalTime();

ofstream resFile;
if(fileExists(argv[4])){
	resFile.open(argv[4], fstream::app|fstream::out);
}
else{
resFile.open(argv[4], fstream::app|fstream::out);
resFile << "\n"
<< setw(15) << left << "Implementation"
<< setw(15) << left << "IsoValue"
<< setw(15) << left << "Time"
<< setw(15) << left << "Visit"
<< setw(15) << left << "Memory"
<< setw(15) << left << "HtoD"
<< setw(15) << left << "DtoH"
<< setw(15) << left << "Cell Ctg"
<< setw(15) << left << "EdgePoint Time"
<< setw(15) << left << "Merging Time"
<< setw(15) << left << "Sort Time"
<< setw(15) << left << "CopyBack Time"
<< setw(15) << left << "Merge"
<< setw(40) << left << "Dataset";
}

resFile << "\n"
<< setw(15) << left << "VTKM-TBB"
<< setw(15) << left << isovalue
<< setw(15) << left << isoGenTime->GetTotalTime()
<< setw(15) << left << visitTime
<< setw(15) << left << "NA"
<< setw(15) << left << "NA"
<< setw(15) << left << "NA"
<< setw(15) << left << ccTime
<< setw(15) << left << epTime
<< setw(15) << left << mgTime
<< setw(15) << left << sortTime
<< setw(15) << left << "NA"
<< setw(15) << left << mergepoints 
<< setw(40) << left << argv[1];

}

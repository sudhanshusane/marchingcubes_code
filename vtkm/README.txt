This repository contains code for RAJA - CUDA and OMP. 
VTKM marching cubes implementations used were from the VTKM examples provided. 
The folder titled 'paper' contains the rough draft of the paper that was targeting ISAV 16. 
The paper contains the performance study results, however, the right message for the comparison being carried out is missing since RAJA is not built targeting visualization algorithms.  

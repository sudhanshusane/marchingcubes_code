This section provides the details of the performance study conducted. 

\subsection{Algorithm}
Contouring is a common visualization operation for analyzing scalar fields.
%
The algorithm creates an isosurface (i.e., a continuous surface), which represents all points of a single constant value (i.e., an isovalue) of a selected scalar field within a three dimensional volume.
%
Marching cubes~\cite{lorensen1987marching} is a contouring algorithm for structured grids, and it creates the isosurface by ''marching'' through each cell in the data set and generating triangles for cells that the isosurface passes through.
%

\subsection{Study Phases and Factors}
We varied the following parameters in our experiments:
\begin{tightItemize}
	\item Implementations (5 options)
%	\item Hardware Architectures (2 options)
	\item Data Sets (6 options)
	\item Isovalues (3 options)
\end{tightItemize}
%
We tested the cross product of all factors, resulting in 108 (i.e., 6x6x3) total tests.
%
Each test generates an isosurface using the Marching Cubes algorithm and we ran each test 10 times and took the average timings over all 10 runs. 
%
The study consisted of two phases detailed below ---
%
%For both phases in the study, we ran each test 10 times and took the average over all 10 runs.
%
%In phase one of our study, we compared the parallel versions of the RAJA and VTK-m implementations. 
%
%
%In phase two of the study, we compared the serial versions of RAJA and VTK-m with VisIt, a community standard.
%The performance study was performed in two phases. 

\begin{tightItemize}
\item \textbf{Phase1:} In this phase we compared the performance of VTK-m and RAJA implementations. 
%
The comparison is of the overall Marching Cubes algorithm timing, which includes cell classification, geometry generation and merging duplicate points. 
%
\item \textbf{Phase2:} In the second phase, we compare timings of VTK-m and RAJA implementations against VisIt.
%
We ran VisIt in serial since the data was not domain decomposed, which is a requirement to use VisIt in parallel. 
% 

VisIt supports a different contour filter implementation, such that it spends a bulk of its time creating a scalar tree, which it caches for use by other filters and future executions of the contour filter.
%
The scalar tree returns a list of cells containing the isosurface value, and once the list is created, the rest of the Marching Cubes algorithm executes in the same manner as the RAJA and VTK-m implementations.
%
Because of this difference, we could not compare the overall times of the algorithm, and we only compared the matching steps of the algorithm.
% 
Thus, we only consider time to generate geometry and merge duplicate points. 
%
Finally, for VisIt, we used it's in-built timing mechanism, whereas for the VTK-m and RAJA implementations we instrumented the different sections of the code. 
\end{tightItemize}
%

\subsubsection{Software Implementation}
Our study compared the performance of two performance portability libraries --- VTK-m and RAJA. 
%We used implementations of marching cubes from VTK-m, RAJA, and VisIt.
%
We used a total of 5 software implementations:
\begin{tightItemize}
	\item VTK-m CUDA 
	\item VTK-m TBB 
%	\item VTK-m Serial 
	\item RAJA CUDA
	\item RAJA OpenMP
	\item VisIt Serial
\end{tightItemize}

%
%The first set of software implementations use the VTK-m Marching Cubes filter, which is part of the VTK-m library.
%
For VTK-m, we used the existing Marching Cubes filter, and created two versions corresponding to the two available parallel back-ends currently available: CUDA and TBB 
%
%For the VTK-m implementation we considered three device adapters - CUDA, TBB and Serial.
For RAJA, we implemented two data parallel versions of marching cubes for CUDA, OpenMP, very similar to the VTK-m implementation.
%
Although additional operations are current under development, RAJA only provides constructs for map operations (i.e., a parallel for loop) and reductions, and RAJA does not manage GPU and CPU memory transfers.
%
We wanted the VTK-m and RAJA implementations mirror each other as much as possible, so we had to augment the RAJA implementations additional parallel scan and sort operations using the Thrust library.
%
Finally, we used the contour filter in VisIt~\cite{childs2005contract}, a commuity standard production visualization tool from Lawrence Livermore National Laboratory.

%
%The second set of software implementations use RAJA for loop parallelization and Thrust for data parallel scans and sorting. 
%
%RAJA currently does not support scan and sorting, although it is planned in the future. 
%
%There were a total of 2 RAJA software implementations ---
%
%Thrust functions used in the above software implementations use the corresponding backends, i.e. Thrust - CUDA and Thrust - OpenMP respectively. 
%

\subsubsection{Hardware Architectures}
All tests were performed on a single compute node of the Surface cluster at Lawrence Livermore National Lab. 
%
Surface is used for visualization and data analysis work.
%
\begin{tightItemize}
%
\item \textbf{CPU:} Each node has an Intel Xeon E5-2670, with 16 cores per node, running at 2.6 Ghz. Each node contains 256 GB of memory. 
%
\item \textbf{GPU:} An NVIDIA K40m, with 2880 CUDA cores, running at 745 MHz. Each GPU contains 12 GB of memory.  
%
\end{tightItemize}

\subsubsection{Data Sets}
Each data set considered in this performance study was mapped onto a 3D rectilinear grid. 
%
There are a total of 6 data sets, the details of which are given below --- 

\begin{tightItemize}
\item \textbf{Supernova:} A 134.2 million hexahedron data set generated by an astrophysics simulation \fix{WHERE}.
%
The grid dimensions were $512^3$. 
%
Isovalues 13, 4 and 1 were selected for this data set.
%
\item \textbf{Nek5000:} A 16.7 million hexahedron sized selection of the original data set generated by the Nek5000 thermal hydraulics simulation \cite{fischer2008nek5000}. 
%
The grid dimensions were $256^3$. 
%
Isovalues 1, 0.6 and 0.4 were selected for this data set.
%
\item \textbf{Rayleigh-Taylor:} We considered different sizes and selections of the very large original data set generated by the Rayleigh Taylor fluid instability simulation \fix{WHERE}. 
%
\begin{tightItemize}
\item \textbf{RT\_Small:} 9.5 million hexahedron data set.
\item \textbf{RT\_Medium:} 38.1 million hexahedron data set.
\item \textbf{RT\_Large:} 76.2 million hexahedron data set.
\item \textbf{RT\_Huge:} 114.4 million hexahedron data set. 
\end{tightItemize}
Each grid had dimensions x = 193 , y = 193 and z = 256, 1024, 2048, 3072 respectively. 
%
Isovalues 9, 5 and 1 were selected for this data set.
\end{tightItemize}

\subsubsection{Isovalues}
We chose 3 isovalues for each data set. 
%
Isovalues are selected such that a small, medium and large amount of geometry is generated within each selected data set. 
%

%
The marching cubes algorithm has identifiable phases such as cell classification and geometry generation. 
%
Cell classification time is affected by the dimensions of the data set grid, whereas geometry generation is affected by the number of points with scalar values equal to the selected isovalue within the data set. 
%
Table \ref{table:isovalue_geometry} details the number of unique points and triangles generated by each data set for a given isovalue. 
\begin{table*}[]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
                    & Isovalue 1    &           & Isovalue 2    &           & Isovalue 3    &           \\ \hline
Data set            & Unique Points & Triangles & Unique Points & Triangles & Unique Points & Triangles \\ \hline
Supernova           & 88796         & 177592    & 268168        & 536332    & 6541830       & 1283656   \\ \hline
Nek5000             & 39975         & 78322     & 280921        & 559276    & 1256924       & 2507654   \\ \hline
RT\_Small            & 8257          & 15996     & 57591         & 112826    & 232159        & 456687    \\ \hline
RT\_Medium           & 307557        & 611006    & 991143        & 1970122   & 1472519       & 2922301   \\ \hline
RT\_Large            & 60826         & 119966    & 681213        & 1351852   & 2178502       & 4328478   \\ \hline
RT\_Huge             & 344809        & 683936    & 1421273       & 2823768   & 2966362       & 5891886   \\ \hline
\end{tabular}
\caption{Data set Isosurfaces Information}
\label{table:isovalue_geometry}
\end{table*}



#include <utility>
#include <algorithm>
#include <iostream>
#include "RAJA/RAJA.hxx"
#include <omp.h>

#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <thrust/sort.h>
#include <thrust/device_ptr.h>

using namespace std;

struct VertexInfo{
	int index, v1, v2;
};



__device__
unsigned int uniquePair(int x, int y)
{
if(x > y)
	return (x*x) + x + y;	
else
	return (y*y) + x;
}

 
class ConnectedTriangles
{
	int *faces;
	float *uniquePoints;
	VertexInfo *vertices;
	unsigned int *mc;
	int totalUniquePoints;
	int totalNumFaces;
	TimingModule *copyT, *parT, *memT, *sortT, *visT, *hT, *dT;

	public:
	
	ConnectedTriangles(){
		totalUniquePoints = 0;
		totalNumFaces = 0;
		parT = new TimingModule();
		memT = new TimingModule();
		sortT = new TimingModule();
		hT = new TimingModule();
		copyT = new TimingModule();	
		dT = new TimingModule();	
		visT = new TimingModule();
	}

	~ConnectedTriangles(){
		delete [] faces;
		delete [] uniquePoints;
	}
	
	void GenerateUniquePoints(float *points, int *edges, int numPoints, float minX, float maxX, float minY, float maxY, float minZ, float maxZ){
		const size_t block_size = 512;

		typedef RAJA::cuda_exec<block_size> execute_policy;
		
		visT->StartTimer();
		
		memT->StartTimer();	
		vertices = new VertexInfo[numPoints];  
		faces = new int[numPoints]; 
		mc = new unsigned int[numPoints];  
		memT->EndTimer();
		memT->AccumulateTotalTime();

		totalNumFaces = numPoints/3;

//		float *d_pts;
		int *d_edges;
	 	VertexInfo *d_vertices;	
		unsigned int *d_mc;
		int *d_flagDuplicates;
		int *d_pointIndicesEnum;
		int *d_faces;
		float *d_points;
		memT->StartTimer();
		gpuErrchk( cudaMalloc((void**)&d_faces, sizeof(int)*numPoints));
		gpuErrchk( cudaMalloc((void**)&d_pointIndicesEnum, sizeof(int)*numPoints) );
		gpuErrchk( cudaMalloc((void**)&d_flagDuplicates, sizeof(int)*numPoints) );
		gpuErrchk( cudaMalloc((void**)&d_vertices, sizeof(VertexInfo)*numPoints) );
		gpuErrchk( cudaMalloc((void**)&d_mc, sizeof(unsigned int)*numPoints) );
		memT->EndTimer();
		memT->AccumulateTotalTime();
		visT->EndTimer();
		visT->AccumulateTotalTime();
	
		copyT->StartTimer();
		gpuErrchk( cudaMalloc((void**)&d_edges, sizeof(int)*2*numPoints) );
		gpuErrchk( cudaMalloc((void**)&d_points, sizeof(float)*3*numPoints));
		gpuErrchk( cudaMemcpy(d_edges, edges, sizeof(int)*numPoints*2, cudaMemcpyHostToDevice) );
		copyT->EndTimer();
		copyT->AccumulateTotalTime();

		visT->StartTimer();	
		RAJA::forall<execute_policy>(0, numPoints, [=] RAJA_DEVICE (int i) {
			d_mc[i] = uniquePair(d_edges[i*2+0], d_edges[i*2+1]);
			d_vertices[i].index = i;
			d_vertices[i].v1 = d_edges[i*2+0];
			d_vertices[i].v2 = d_edges[i*2+1];
		});
		visT->EndTimer();
		visT->AccumulateTotalTime();

		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(vertices, d_vertices, sizeof(VertexInfo)*numPoints, cudaMemcpyDeviceToHost) );
		gpuErrchk( cudaMemcpy(mc, d_mc, sizeof(unsigned int)*numPoints, cudaMemcpyDeviceToHost) );
		copyT->EndTimer();
		copyT->AccumulateTotalTime();		

		sort(mc, vertices, numPoints);

		copyT->StartTimer();	
		gpuErrchk( cudaMemcpy(d_mc, mc, sizeof(unsigned int)*numPoints, cudaMemcpyHostToDevice) );
		gpuErrchk( cudaMemcpy(d_vertices, vertices, sizeof(VertexInfo)*numPoints, cudaMemcpyHostToDevice) ); // Update it to the sorted vertex list.
		copyT->EndTimer();
		copyT->AccumulateTotalTime();

	
		visT->StartTimer();
		RAJA::forall<execute_policy>(0, numPoints, [=] RAJA_DEVICE (int i) {
			int flag = 1;
		        if(d_mc[i] == d_mc[i+1])
		                 if(d_vertices[i].v1 == d_vertices[i+1].v1 && d_vertices[i].v2 == d_vertices[i+1].v2 ||
		                    d_vertices[i].v1 == d_vertices[i+1].v2 && d_vertices[i].v2 == d_vertices[i+1].v1)
	                                flag = 0;
		        d_flagDuplicates[i+1] = flag;
			if(i == 0)
				d_flagDuplicates[0] = 1;
		});
	//	visT->EndTimer();
	//	visT->AccumulateTotalTime();
	

		int h_pointIndicesEnum;
							


		thrust::device_ptr<int> dev_pointIndicesEnum(d_pointIndicesEnum);
		thrust::device_ptr<int> dev_flagDuplicates(d_flagDuplicates);
		
	//	visT->StartTimer();

		thrust::exclusive_scan(dev_flagDuplicates, dev_flagDuplicates + numPoints, dev_pointIndicesEnum);

	//	visT->EndTimer();
	//	visT->AccumulateTotalTime();
		dT->StartTimer();
		gpuErrchk( cudaMemcpy(&h_pointIndicesEnum, &d_pointIndicesEnum[numPoints-1], sizeof(int), cudaMemcpyDeviceToHost) );
		dT->EndTimer();
		dT->AccumulateTotalTime();


		totalUniquePoints = h_pointIndicesEnum; // No need to add 1 when using thrust:exclusive_Scan
	

		
	//	visT->StartTimer();
		RAJA::forall<execute_policy>(0, numPoints, [=] RAJA_DEVICE (int i) {
			int faceNo = d_vertices[i].index/3;
			int vertexNo = d_vertices[i].index%3;
			d_faces[faceNo*3 + vertexNo] = d_pointIndicesEnum[i];
		});
		visT->EndTimer();
		visT->AccumulateTotalTime();

		gpuErrchk( cudaPeekAtLastError() );
		
		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(faces, d_faces, sizeof(int)*numPoints, cudaMemcpyDeviceToHost));
		copyT->EndTimer();
		copyT->AccumulateTotalTime();
		visT->StartTimer();
		
		int *d_uniquePointIndices;
		memT->StartTimer();
		gpuErrchk( cudaMalloc((void**)&d_uniquePointIndices, sizeof(int)*totalUniquePoints) );
		memT->EndTimer();	
	
		RAJA::forall<execute_policy>(0, numPoints, [=] RAJA_DEVICE (int i) {
			if(i != 0 && i > 0){
				if(d_pointIndicesEnum[i] > d_pointIndicesEnum[i-1])
					d_uniquePointIndices[d_pointIndicesEnum[i]] = i;
			}
			else{
				d_uniquePointIndices[0] = 0;
			}
		});

		float *d_uniquePoints;
	
		memT->StartTimer();
		uniquePoints = new float[totalUniquePoints*3];
		gpuErrchk( cudaMalloc((void**)&d_uniquePoints, sizeof(float)*totalUniquePoints*3) );
		memT->EndTimer();
		memT->AccumulateTotalTime();
		visT->EndTimer();
		visT->AccumulateTotalTime();
		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(d_points, points, sizeof(float)*3*numPoints, cudaMemcpyHostToDevice) );
		copyT->EndTimer();
		copyT->AccumulateTotalTime();
		
	
		visT->StartTimer();
		RAJA::forall<execute_policy>(0, totalUniquePoints, [=] RAJA_DEVICE (int i) {
			int idx1 = d_uniquePointIndices[i];
			int idx2 = d_vertices[idx1].index;
			d_uniquePoints[i*3 + 0] = d_points[idx2*3 + 0];
			d_uniquePoints[i*3 + 1] = d_points[idx2*3 + 1];
			d_uniquePoints[i*3 + 2] = d_points[idx2*3 + 2];
			
		});
		visT->EndTimer();
		visT->AccumulateTotalTime();
	

		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(uniquePoints, d_uniquePoints, sizeof(float)*3*totalUniquePoints, cudaMemcpyDeviceToHost));
		copyT->EndTimer();
		copyT->AccumulateTotalTime();

		memT->StartTimer();
		gpuErrchk( cudaFree(d_pointIndicesEnum) );
		gpuErrchk( cudaFree(d_faces) );
		gpuErrchk( cudaFree(d_edges) );
		gpuErrchk( cudaFree(d_flagDuplicates) );
		gpuErrchk( cudaFree(d_points) );
		gpuErrchk( cudaFree(d_uniquePoints) );
		gpuErrchk( cudaFree(d_uniquePointIndices) );
		gpuErrchk( cudaFree(d_vertices) );

		delete [] mc;
		delete [] vertices;
		memT->EndTimer();
		memT->AccumulateTotalTime();
//		visT->EndTimer();
//		visT->AccumulateTotalTime();	
	}

	int* GetFaceList(){
		return faces;
	}

	float* GetUniquePoints(){
		return uniquePoints;
	}

	int GetNumberOfUniquePoints(){
		return totalUniquePoints;
	}

	int GetNumberOfFaces(){
		return totalNumFaces;
	}
	
	inline float GetCopyTime(){
		return copyT->GetTotalTime();
	}

	inline float GetParallelTime(){
		return parT->GetTotalTime();
	}

	inline float GetMemoryTime(){
		return memT->GetTotalTime();
	}
	
	inline double GetHToDTime(){
		return hT->GetTotalTime();
	}
	
	inline double GetDToHTime(){
		return dT->GetTotalTime();
	}

	inline float GetVisitTime(){
		return visT->GetTotalTime();
	}

	inline float GetSortTime(){
		return sortT->GetTotalTime();
	}
		
	private:
		
	inline void sort(unsigned int *mc, VertexInfo *vertices, int n){
		unsigned int *d_mc;
		VertexInfo *d_vertices;
		memT->StartTimer();
		gpuErrchk( cudaMalloc((void**)&d_mc, sizeof(unsigned int)*n) );
		gpuErrchk( cudaMalloc((void**)&d_vertices, sizeof(VertexInfo)*n) );
		memT->EndTimer();
		memT->AccumulateTotalTime();
		
		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(d_mc, mc, sizeof(unsigned int)*n, cudaMemcpyHostToDevice) );
		gpuErrchk( cudaMemcpy(d_vertices, vertices, sizeof(VertexInfo)*n, cudaMemcpyHostToDevice) );
		copyT->EndTimer();
		copyT->AccumulateTotalTime();
	
		thrust::device_ptr<unsigned int> dev_mc(d_mc);
		thrust::device_ptr<VertexInfo> dev_vertices(d_vertices);
		sortT->StartTimer();
		visT->StartTimer();
	
		thrust::sort_by_key(dev_mc, dev_mc + n, dev_vertices);
		visT->EndTimer();
		visT->AccumulateTotalTime();
		sortT->EndTimer();
		sortT->AccumulateTotalTime();
	
		copyT->StartTimer();
		gpuErrchk( cudaMemcpy(mc, d_mc, sizeof(unsigned int)*n, cudaMemcpyDeviceToHost ) );
		gpuErrchk( cudaMemcpy(vertices, d_vertices, sizeof(VertexInfo)*n, cudaMemcpyDeviceToHost ) );
		copyT->EndTimer();
		copyT->AccumulateTotalTime();
	}





};

cmake_minimum_required (VERSION 2.8)

SET(PROJECTNAME main)

PROJECT (${PROJECTNAME})

SET(PROJECT_SRC main.cpp)

SET(ENABLE_CUDA ON)

SET(VTK_DIR /g/g90/sane2/VTK/VTK-build/)
find_package(VTK REQUIRED) #vtk.cmake

SET(RAJA_DIR /g/g90/sane2/RAJA/cuda_build/build/share/cmake/raja/)
find_package(RAJA REQUIRED) #looks for RAJA.cmake

SET(CUDA_TOOLKIT_ROOT_DIR /opt/cudatookkit-7.5/)
find_package(CUDA REQUIRED) # maybe in cuda.cmake there is a CUDA_TOOLKIT_ROOT_DIR?

#set(VARIABLE "Hello World!" CACHE STRING "")
#set(VARIABLE ${VARIABLE} CACHE STRING "Description." FORCE)

SET(CMAKE_CXX_FLAGS "-fopenmp -g" CACHE STRING "Setting CMAKE_CXX_FLAGS." FORCE)
SET(CUDA_NVCC_FLAGS "-O2 -g -lineinfo" CACHE STRING "SETTING CUDA_NVCC_FLAGS." FORCE)
include(../../../RAJA/cmake/modules/FindCUDA.cmake)

#include(FindCUDA)

set(RAJA_FOUND TRUE)
if(ENABLE_CUDA)
	SET(RAJA_CUDA_FLAGS "-std c++11 --restrict -arch=compute_35 --expt-extended-lambda --x=cu" CACHE STRING "Setting RAJA_CUDA_FLAGS." FORCE)
endif()

include_directories(${VTK_INCLUDE_DIRS})
include_directories(${CUDA_INCLUDE_DIRS})
include_directories(${RAJA_INCLUDE_DIRS}) #empty variable?
include_directories(/g/g90/sane2/RAJA/cuda_build/build/include) #includes RAJA/RAJA.hxx, RAJA/config.hxx

#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS},${RAJA_CUDA_FLAGS}")
SET(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} ${RAJA_CUDA_FLAGS}" CACHE STRING "Modifying CUDA_NVCC_FLAGS." FORCE)
#SET(CMAKE_CXX_FLAGS_PREV ${CMAKE_CXX_FLAGS})

CUDA_ADD_EXECUTABLE(${PROJECTNAME} ${PROJECT_SRC})

#SET(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS_PREV})

target_include_directories(${PROJECTNAME} PRIVATE ${RAJA_INCLUDE_PATH} ${CUDA_INCLUDE_DIRS})

target_link_libraries(${PROJECTNAME} ${CUDA_LIBRARIES} ${RAJA})

#target_link_libraries(${PROJECTNAME} ${RAJA})

if(VTK_LIBRARIES)
 target_link_libraries(${PROJECTNAME} ${VTK_LIBRARIES})
else()
 target_link_libraries(${PROJECTNAME} vtkHybrid)
endif()

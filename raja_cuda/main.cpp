#include "IsosurfaceGenerator_CUDA.cu"
#include "ConnectedTriangles_CUDA.cu"
#include "TimingModule.h"
#include <iomanip>
#include <vtkDataSetReader.h>
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>

#include <iostream>
#include <sys/time.h>
#include <sys/stat.h>

using namespace std;

bool fileExists(char *filename)
{
	string str(filename);
	struct stat buf;
	if(stat(str.c_str(), &buf) != -1)
		return true;
	return false;
}

int main(int argc, char *argv[])
{
	if(argc != 7){
		cout << "Insufficient arguments.\n Usage : " << argv[0] << " <dataset_filename> <isoval> <fieldName> <results_filename> <merge-1/0> <output_filename>\n";
	}	

	TimingModule *isoT = new TimingModule();
	TimingModule *connT = new TimingModule();
	TimingModule *totT = new TimingModule();


	vtkDataSetReader *rdr = vtkDataSetReader::New();
	rdr->SetFileName(argv[1]);
	rdr->SetScalarsName(argv[3]);	
	rdr->Update();
		
	int dims[3];
	vtkRectilinearGrid *rgrid = (vtkRectilinearGrid *)rdr->GetOutput();
	rgrid->GetDimensions(dims);
	
	float *X = (float *) rgrid->GetXCoordinates()->GetVoidPointer(0);
	float *Y = (float *) rgrid->GetYCoordinates()->GetVoidPointer(0);
	float *Z = (float *) rgrid->GetZCoordinates()->GetVoidPointer(0);
	float *F = (float *) rgrid->GetPointData()->GetScalars()->GetVoidPointer(0);
	float isoVal = atof(argv[2]);
	int merge = atoi(argv[5]);
	bool mergepoints;

	if(merge == 1)
		mergepoints = true;
	else
		mergepoints = false;

	totT->StartTimer();

/*
 * Generate set of points and edges.
 */	
	IsosurfaceGenerator *isoGen = new IsosurfaceGenerator();
	isoGen->GenerateIsosurface(X,Y,Z,F,dims,isoVal);
	
/*
 * Generate the unique set of points by merging duplicates. 
 */
	ConnectedTriangles *ct = new ConnectedTriangles();
	if(mergepoints == 1)
	ct->GenerateUniquePoints(isoGen->GetPointList(), isoGen->GetEdgeList(), isoGen->GetNumberOfPoints(), 0, dims[0], 0, dims[1], 0, dims[2]);
	
	totT->EndTimer();
	totT->AccumulateTotalTime();


/*
 * Write the list of vertices and faces to a file. 
 */

	vtkPolyData *pd = isoGen->MakePolyData(ct->GetFaceList(), ct->GetUniquePoints(), ct->GetNumberOfFaces(), ct->GetNumberOfUniquePoints());

	vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
	writer->SetFileName(argv[6]);

	writer->SetInputData(pd);
	writer->SetDataModeToAscii();
	writer->Write();
	ofstream resFile;
	
/*
 *  Performance timings for individual sections of the code. 
 */ 
	double visitTime = isoGen->GetVisitTime() + ct->GetVisitTime();
	double memTime = isoGen->GetMemoryTime() + ct->GetMemoryTime();
	double htodTime = isoGen->GetHToDTime() + ct->GetHToDTime();
	double dtohTime = isoGen->GetDToHTime() + ct->GetDToHTime();
	double ccTime = isoGen->GetCCTime();
	double epTime = isoGen->GetVisitTime();
	double mgTime = ct->GetVisitTime();
	double sortTime = ct->GetSortTime();
	double copyTime = isoGen->GetCopyTime() + ct->GetCopyTime();
	
	if(fileExists(argv[4])){
		resFile.open(argv[4], fstream::app|fstream::out);	
	}
	else{
	resFile.open(argv[4], fstream::app|fstream::out);       
	resFile << "\n" 
	<< setw(15) << left << "Implementation"
	<< setw(15) << left << "IsoValue" 
	<< setw(15) << left << "Time" 
	<< setw(15) << left << "Visit" 
	<< setw(15) << left << "Memory" 
	<< setw(15) << left << "HtoD" 
	<< setw(15) << left << "DtoH" 
	<< setw(15) << left << "Cell Ctg" 
	<< setw(15) << left << "EdgePoint Time" 
	<< setw(15) << left << "Merge Time" 
	<< setw(15) << left << "Sort Time" 
	<< setw(15) << left << "CopyBack Time" 
	<< setw(15) << left << "Merge" 
	<< setw(40) << left << "Dataset"; 
	}
	
	resFile << "\n" 
	<< setw(15) << left << "RAJA-CUDA"
	<< setw(15) << left << isoVal 
	<< setw(15) << left << (totT->GetTotalTime() - copyTime )
	<< setw(15) << left << visitTime 
	<< setw(15) << left << memTime 
	<< setw(15) << left << htodTime 
	<< setw(15) << left << dtohTime 
	<< setw(15) << left << ccTime 
	<< setw(15) << left << epTime 
	<< setw(15) << left << mgTime 
	<< setw(15) << left << sortTime 
	<< setw(15) << left << copyTime 
	<< setw(15) << left << mergepoints
	<< setw(40) << left << argv[1]; 
}






#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkDataArray.h>

#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

#include "triTable.cu"
#include "numTrianglesTable.cu"
#include "interpolateTable.cu"
#include "TimingModule.h"
#include "RAJA/RAJA.hxx"

#include <thrust/scan.h>
#include <thrust/device_ptr.h>

#define BPG 240
#define TPB 512

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__host__ __device__ 
inline int GetNumberOfCells(int *dims)
{
	return (dims[0]-1) * (dims[1]-1) * (dims[2]-1);
}


__device__ 
inline void GetLogicalCellIndex(int *idx, int cellId, const int *dims)
{       
        idx[0] = cellId%(dims[0]-1);
        idx[1] = (cellId/(dims[0]-1))%(dims[1]-1);
        idx[2] = cellId/((dims[0]-1)*(dims[1]-1));
}

		
__device__
inline void AddPoint(float *points, float *pt, int idx)
{
	points[idx+0] = pt[0];
	points[idx+1] = pt[1];
	points[idx+2] = pt[2];
}

__device__ 
inline void GetPointValues(int x, int y, int z, const float *F, float *values, int *dims)
{

	values[0] = F[z*dims[0]*dims[1] + y*dims[0] + x];
	values[1] = F[z*dims[0]*dims[1] + y*dims[0] + (x+1)];
	values[2] = F[(z)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1)];
	values[3] = F[(z)*dims[0]*dims[1] + (y+1)*dims[0] + (x)];
	values[4] = F[(z+1)*dims[0]*dims[1] + y*dims[0] + x];
	values[5] = F[(z+1)*dims[0]*dims[1] + y*dims[0] + (x+1)];
	values[6] = F[(z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1)];
	values[7] = F[(z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x];
	
}


__device__ 
inline void GetPositionOnEdge(float Ax, float Bx, float Ay, float By, float Az, float Bz, float FA, float FB, float *pt, float isoVal)
{
	float dif1 = (isoVal - FA);
	float dif2 = (FB - FA);
	float t = dif1/dif2;
	pt[0] = t*(Bx-Ax) + Ax;
	pt[1] = t*(By-Ay) + Ay;
	pt[2] = t*(Bz-Az) + Az;
}


__device__ 
inline void InterpolatePosition(int edge, float *pt, float *bbox, float *v, float isoVal, int *ids)
{
float bbox_1 = bbox[ip[edge*8+0]];
float bbox_2 = bbox[ip[edge*8+1]];
float bbox_3 = bbox[ip[edge*8+2]];
float bbox_4 = bbox[ip[edge*8+3]];
float bbox_5 = bbox[ip[edge*8+4]];
float bbox_6 = bbox[ip[edge*8+5]];
float v_1 = v[ip[edge*8+6]];
float v_2 = v[ip[edge*8+7]];

GetPositionOnEdge(bbox_1, bbox_2, bbox_3, bbox_4, bbox_5, bbox_6, v_1, v_2, pt, isoVal);
}

__device__
inline void GetPointId(int *ids, int *dims, int x, int y, int z){
        ids[0] = z*dims[0]*dims[1] + y*dims[0] + x;
        ids[1] = z*dims[0]*dims[1] + y*dims[0] + (x+1);
        ids[2] = (z)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
        ids[3] = (z)*dims[0]*dims[1] + (y+1)*dims[0] + (x);
        ids[4] = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
        ids[5] = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
        ids[6] = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
	ids[7] = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
}


__device__ 
inline void BoundingBoxForCell(const float *X, const float *Y, const float *Z, int *dims, int cellId, float *bbox)
{
	if(cellId >= 0 && cellId < GetNumberOfCells(dims)){
      		int idx[3];
		GetLogicalCellIndex(idx, cellId, dims);
     			bbox[0] = X[idx[0]];
       			bbox[1] = X[idx[0]+1];
               		bbox[2] = Y[idx[1]];
               		bbox[3] = Y[idx[1]+1];
               		bbox[4] = Z[idx[2]];
               		bbox[5] = Z[idx[2]+1];
 	}
       	else{
	        bbox[0] = -10000;
  		bbox[1] = +10000;
	      	bbox[2] = -10000;
	        bbox[3] = +10000;
               	bbox[4] = -10000;
	        bbox[5] = +10000;
   	}
}


__device__ 
inline int IdentifyCellCase(int index, int *dims, float isoVal, float *F){
	float values[8];
	int v[8] = {0};
	int idx[3];
	GetLogicalCellIndex(idx, index, dims);
	GetPointValues(idx[0], idx[1], idx[2], F, values, dims);
	for(int i = 0; i < 8 ; i ++)
	        if(values[i] > isoVal)
	                v[i] = 1;
	return (v[7]*128 + v[6]*64 + v[5]*32 + v[4]*16 + v[3]*8 + v[2]*4 + v[1]*2 + v[0]*1);
}

__device__
inline void UpdateEdges(int *dims, int edge1, int x, int y, int z, int edgeIndex, int offset, int *edges){
int ptId1, ptId2;
if(edge1 == 0){
	ptId1= z*dims[0]*dims[1] + y*dims[0] + x;
        ptId2= z*dims[0]*dims[1] + y*dims[0] + (x+1);
}
else if(edge1 == 1){
        ptId1 = z*dims[0]*dims[1] + y*dims[0] + (x+1);
        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
}
else if(edge1 == 2){
        ptId1 = z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + x;
}
else if(edge1 == 3){
        ptId1 = z*dims[0]*dims[1] + y*dims[0] + x;
        ptId2 = z*dims[0]*dims[1] + (y+1)*dims[0] + x;
}
else if(edge1 == 4){
       ptId1 = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
       ptId2 = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
}
else if(edge1 == 5){
	ptId1 = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
	ptId2= (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
}
else if(edge1 == 6){
      ptId1  =     (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
      ptId2  = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
}
else if(edge1 == 7){
     ptId1   =     (z+1)*dims[0]*dims[1] + y*dims[0] + x;
     ptId2   = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
}
else if(edge1 == 8){
      ptId1  =     z*dims[0]*dims[1] + y*dims[0] + x;
      ptId2  = (z+1)*dims[0]*dims[1] + y*dims[0] + x;
}
else if(edge1 == 9){
      ptId1  =     z*dims[0]*dims[1] + y*dims[0] + (x+1);
      ptId2  = (z+1)*dims[0]*dims[1] + y*dims[0] + (x+1);
}
else if(edge1 == 10){
      ptId1  =     z*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
      ptId2  = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + (x+1);
}
else if(edge1 == 11){
   ptId1     =     z*dims[0]*dims[1] + (y+1)*dims[0] + x;
   ptId2     = (z+1)*dims[0]*dims[1] + (y+1)*dims[0] + x;
}

if( ptId1 > ptId2)
{
	int temp =ptId1;
	ptId1 = ptId2;
	ptId2 = temp;
}
        edges[edgeIndex+0+offset] = ptId1;
        edges[edgeIndex+1+offset] = ptId2;
}


class IsosurfaceGenerator
{
	int *edges;	
	float* points;
	int totalNumVertices;
	int maxFaces;
	int maxPoints;
	int totalPts;
	TimingModule *ccT, *memT, *visT , *hT, *dT, *copyT;

public:
	IsosurfaceGenerator(){
		ccT = new TimingModule();
		memT = new TimingModule();
		visT = new TimingModule();
		hT = new TimingModule();
		copyT = new TimingModule();
		dT = new TimingModule();
	}
	
	~IsosurfaceGenerator(){
		delete [] points;
	}
		
void GenerateIsosurface(float *X, float *Y, float *Z, float *F, int *dims, float isoVal){
	const size_t block_size = 512;
	typedef RAJA::cuda_exec<block_size> execute_policy;	
		
	int numberOfCells =(dims[0]-1) * (dims[1]-1) * (dims[2]-1);
	
	totalPts = dims[0]*dims[1]*dims[2];

	int *d_dims, *d_numFacesPerCell, *d_validCell;
	float *d_F;
	int h_validCellEnum;
	int *d_validCellEnum;
	int sizeX = dims[0];
	int sizeY = dims[1];
	int sizeZ = dims[2];
	float *d_X, *d_Y, *d_Z;
	int dummyVar;
	
	copyT->StartTimer();
	gpuErrchk( cudaMalloc((void**)&dummyVar, sizeof(int)) ) ;
	copyT->EndTimer();
	copyT->AccumulateTotalTime();	
	
	memT->StartTimer();			
	gpuErrchk( cudaMalloc((void**)&d_F, sizeof(float)*totalPts) ) ;
	gpuErrchk( cudaMalloc((void**)&d_dims, 3*sizeof(int)) );
	gpuErrchk( cudaMalloc((void**)&d_numFacesPerCell, numberOfCells*sizeof(int)) ) ;
	gpuErrchk( cudaMalloc((void**)&d_validCell, numberOfCells*sizeof(int)) ); 
	gpuErrchk( cudaMalloc((void**)&d_validCellEnum, numberOfCells*sizeof(int)) ); 
	gpuErrchk( cudaMalloc((void**)&d_X, sizeX*sizeof(float)) );
	gpuErrchk( cudaMalloc((void**)&d_Y, sizeY*sizeof(float)) );
	gpuErrchk( cudaMalloc((void**)&d_Z, sizeZ*sizeof(float)) );
	memT->EndTimer();
	memT->AccumulateTotalTime();

	hT->StartTimer();	
	gpuErrchk( cudaMemcpy(d_dims, dims, 3*sizeof(int), cudaMemcpyHostToDevice) ) ;
	gpuErrchk( cudaMemcpy(d_F, F, sizeof(float)*totalPts, cudaMemcpyHostToDevice) );
	hT->EndTimer();
	hT->AccumulateTotalTime();
	ccT->StartTimer();	
	RAJA::forall<execute_policy>(0, numberOfCells, [=] RAJA_DEVICE (int i) {
		int icase, flag = 0;
	 	icase = IdentifyCellCase(i, d_dims, isoVal, d_F);
		d_numFacesPerCell[i] = numTriangles[icase];
		if(numTriangles[icase] > 0)
			flag = 1;
		d_validCell[i] = flag;		
	});

	gpuErrchk( cudaPeekAtLastError() );	

	thrust::device_ptr<int> dev_validCell(d_validCell);
	thrust::device_ptr<int> dev_validCellEnum(d_validCellEnum);

	thrust::inclusive_scan(dev_validCell, dev_validCell + numberOfCells, dev_validCellEnum);
	ccT->EndTimer();
	ccT->AccumulateTotalTime();

	memT->StartTimer();
	gpuErrchk(cudaFree(d_validCell));
	memT->EndTimer();
	memT->AccumulateTotalTime();

	dT->StartTimer();
	gpuErrchk(cudaMemcpy(&h_validCellEnum, &d_validCellEnum[numberOfCells-1], sizeof(int), cudaMemcpyDeviceToHost) );
	dT->EndTimer();
	dT->AccumulateTotalTime();

	int numberOfValidCells = h_validCellEnum;
			
	int *d_validCellIndices;
	
	memT->StartTimer();
	gpuErrchk( cudaMalloc((void**)&d_validCellIndices, (numberOfValidCells*sizeof(int))) );
	memT->EndTimer();
	memT->AccumulateTotalTime();
	ccT->StartTimer();
	RAJA::forall<execute_policy>(0, numberOfCells, [=] RAJA_DEVICE (int i) {
		if(i != 0 && i > 0)
		{
			int a = d_validCellEnum[i];
			int b = d_validCellEnum[i-1];
		//	if(d_validCellEnum[i] > d_validCellEnum[i-1])
			if(a > b)
				d_validCellIndices[d_validCellEnum[i-1]] = i;
		}
		else{
			d_validCellIndices[0] = i; 
		}
	});
	ccT->EndTimer();
	ccT->AccumulateTotalTime();
	int *d_numberOfTrianglesCompacted;

	memT->StartTimer();	
	gpuErrchk(cudaFree(d_validCellEnum));
	int h_numberOfTrianglesCompacted;			
	gpuErrchk( cudaMalloc((void**)&d_numberOfTrianglesCompacted, (numberOfValidCells*sizeof(int))) );
	memT->EndTimer();
	memT->AccumulateTotalTime();
	
	ccT->StartTimer();
	RAJA::forall<execute_policy>(0, numberOfValidCells, [=] RAJA_DEVICE (int i) {
		d_numberOfTrianglesCompacted[i] = d_numFacesPerCell[d_validCellIndices[i]];
	});
	ccT->EndTimer();
	ccT->AccumulateTotalTime();
	gpuErrchk( cudaPeekAtLastError() );

	memT->StartTimer();
		
	gpuErrchk( cudaFree(d_numFacesPerCell) );
	int h_numberOfTrianglesEnum;
	int *d_numberOfTrianglesEnum;
	gpuErrchk( cudaMalloc((void**)&d_numberOfTrianglesEnum, (numberOfValidCells*sizeof(int))) );
	
	memT->EndTimer();
	memT->AccumulateTotalTime();
	ccT->StartTimer();

	thrust::device_ptr<int> dev_numTriCompact(d_numberOfTrianglesCompacted);
	thrust::device_ptr<int> dev_numTriEnum(d_numberOfTrianglesEnum);

	thrust::exclusive_scan(dev_numTriCompact, dev_numTriCompact + numberOfValidCells, dev_numTriEnum);
	
	dT->StartTimer();
	gpuErrchk(cudaMemcpy(&h_numberOfTrianglesEnum, &d_numberOfTrianglesEnum[numberOfValidCells-1],sizeof(int), cudaMemcpyDeviceToHost) );	 
	gpuErrchk( cudaMemcpy(&h_numberOfTrianglesCompacted, &d_numberOfTrianglesCompacted[numberOfValidCells-1], sizeof(int), cudaMemcpyDeviceToHost) );
	dT->EndTimer();
	dT->AccumulateTotalTime();

	totalNumVertices = (h_numberOfTrianglesEnum + h_numberOfTrianglesCompacted)*3;
	
	ccT->EndTimer();
	ccT->AccumulateTotalTime();

	memT->StartTimer();
	
	gpuErrchk( cudaFree(d_numberOfTrianglesCompacted) );

	edges = new int[totalNumVertices*2];
	points = new float[totalNumVertices*3];// x,y,z coordinates
	memT->EndTimer();
	memT->AccumulateTotalTime();

	int *d_edges; 
	float *d_points;	
	
	memT->StartTimer();
	
 	gpuErrchk( cudaMalloc((void**)&d_points, sizeof(float)*3*totalNumVertices) );
	gpuErrchk( cudaMalloc((void**)&d_edges, sizeof(int)*2*totalNumVertices) );
	memT->EndTimer();
	memT->AccumulateTotalTime();

	hT->StartTimer();
	gpuErrchk( cudaMemcpy(d_X, X, sizeX*sizeof(float), cudaMemcpyHostToDevice) );	 
	gpuErrchk( cudaMemcpy(d_Y, Y, sizeY*sizeof(float), cudaMemcpyHostToDevice) );	 
	gpuErrchk( cudaMemcpy(d_Z, Z, sizeZ*sizeof(float), cudaMemcpyHostToDevice) );	 
	hT->EndTimer();
	hT->AccumulateTotalTime();

	visT->StartTimer();
	RAJA::forall<execute_policy>(0, numberOfValidCells, [=] RAJA_DEVICE (int index) {
	float bbox[6];
	float v[8];
	int logIdx[3];
	int edge1, edge2, edge3;
	int e = 0;
	float pt1[3], pt2[3], pt3[3];
	int icase;
	int ids[8];
	int cellId = d_validCellIndices[index];
	BoundingBoxForCell(d_X, d_Y, d_Z, d_dims, cellId, bbox);	
	GetLogicalCellIndex(logIdx, cellId, d_dims);
	GetPointValues(logIdx[0], logIdx[1], logIdx[2], d_F, v, d_dims);
	GetPointId(ids, d_dims, logIdx[0], logIdx[1], logIdx[2]);
	icase = IdentifyCellCase(cellId, d_dims, isoVal, d_F);
	int edgeIndex = d_numberOfTrianglesEnum[index]*6;
	int startIndex = d_numberOfTrianglesEnum[index]*9;
	while(triCase[icase * 16 + e] != -1){
		edge1 = triCase[icase * 16 + (e+0)];
		edge2 = triCase[icase * 16 + (e+1)];
		edge3 = triCase[icase * 16 + (e+2)];
		e += 3;
		UpdateEdges(d_dims, edge1, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 0, d_edges);
		UpdateEdges(d_dims, edge2, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 2, d_edges);
		UpdateEdges(d_dims, edge3, logIdx[0], logIdx[1], logIdx[2], edgeIndex, 4, d_edges);
		edgeIndex += 6;
		InterpolatePosition(edge1, pt1, bbox, v, isoVal, ids);
		InterpolatePosition(edge2, pt2, bbox, v, isoVal, ids);
		InterpolatePosition(edge3, pt3, bbox, v, isoVal, ids);
		AddPoint(d_points, pt1, startIndex);
		startIndex += 3;
		AddPoint(d_points, pt2, startIndex);
		startIndex += 3;
		AddPoint(d_points, pt3, startIndex);
		startIndex += 3;
	}					
	});
	visT->EndTimer();
	visT->AccumulateTotalTime();

	gpuErrchk( cudaGetLastError() );
	
	copyT->StartTimer();
	gpuErrchk( cudaMemcpy(points, d_points, sizeof(float)*totalNumVertices*3, cudaMemcpyDeviceToHost) );
	gpuErrchk( cudaMemcpy(edges, d_edges, sizeof(int)*totalNumVertices*2, cudaMemcpyDeviceToHost) );
	copyT->EndTimer();
	copyT->AccumulateTotalTime();

	memT->StartTimer();

	gpuErrchk( cudaFree(d_X) );
	gpuErrchk( cudaFree(d_Y) );
	gpuErrchk( cudaFree(d_Z) );
	gpuErrchk( cudaFree(d_dims) );
	gpuErrchk( cudaFree(d_numberOfTrianglesEnum) );
	gpuErrchk( cudaFree(d_points) );
	gpuErrchk( cudaFree(d_edges) );
	gpuErrchk( cudaFree(d_validCellIndices) );		

	memT->EndTimer();
	memT->AccumulateTotalTime();

}

		inline int GetNumberOfPoints(){
			return totalNumVertices;
		}
		
		inline int GetNumberOfFaces(){
			return (totalNumVertices/3);
		}
		
		inline float* GetPointList(){
			return points;
		}
		
		inline int* GetEdgeList(){
			return edges;
		}

		inline int GetNumberOfGridPoints(){
			return totalPts;
		}
		
		inline float GetCopyTime(){
			return copyT->GetTotalTime();
		}

		inline float GetCCTime(){
			return ccT->GetTotalTime();
		}
		
		inline float GetHToDTime(){
			return hT->GetTotalTime();
		}
		
		inline float GetDToHTime(){
			return dT->GetTotalTime();
		}

		inline float GetVisitTime(){
			return visT->GetTotalTime();
		}

		inline float GetMemoryTime(){
			return memT->GetTotalTime();
		}

		inline vtkPolyData* MakePolyData(int *faces, float *uniquePoints, int numFaces, int numUniquePoints)
		{
			vtkPoints *vtk_pts = vtkPoints::New();
			vtk_pts->SetNumberOfPoints(numUniquePoints);
				
			vtkCellArray *triangles = vtkCellArray::New();
			triangles->EstimateSize(numFaces, 3);
			
			float pt[3];
			
			for(int i = 0; i < numUniquePoints; i++ ){
				pt[0] = uniquePoints[i*3+0];
				pt[1] = uniquePoints[i*3+1];
				pt[2] = uniquePoints[i*3+2];
				vtk_pts->SetPoint(i, pt);
			}


			for(int i = 0; i < numFaces ; i++ ){
				vtkIdType ids[3] = {faces[i*3+0], faces[i*3+1], faces[i*3+2]};
				triangles->InsertNextCell(3, ids);
			}	
			
			vtkPolyData *pd = vtkPolyData::New();
			pd->SetPoints(vtk_pts);
			pd->SetPolys(triangles);
			
			triangles->Delete();
			vtk_pts->Delete();
			
			return pd;
		}
};
